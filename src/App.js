import React from 'react';
import './App.css';

class App extends React.Component {
  constructor(props){
    super(props)
    this.state = {show:true, showH1:true, showDiv:true, showH2:true, showH3:true, showB:true}
    
  }

  hideButton = () => {
    this.setState({show:false})       
  }

  showButton = () => {
    this.setState({show:true})    
  }

  hideButtonH1 = () => {
    this.setState({showH1:false})
  }

  showButtonH1 = () => {
    this.setState({showH1:true})
  }

  hideButtonDiv = () => {
    this.setState({showDiv:false})
  }

  showButtonDiv = () => {
    this.setState({showDiv:true})
  }

  hideButtonH2 = () => {
    this.setState({showH2:false})
  }

  showButtonH2 = () => {
    this.setState({showH2:true})
  }

  hideButtonH3 = () => {
    this.setState({showH3:!this.state.showH3})
  }

  hideButtonB = () => {
    this.setState({showB:!this.state.showB})
  }
  

  render() {
    return (
      <div>
        <button onClick={this.hideButton}>Esconda Div</button>
        <button onClick={this.showButton}>Mostra Div</button>
        {
          this.state.show && 
          <p>
            Olá React!
          </p>        
        }
        <br />
        <button onClick={this.hideButtonH1}>Esconda H1</button> 
        <button onClick={this.showButtonH1}>Mostra H1</button>
        {
          this.state.showH1 &&
          <h1>
            Olá, H1!
          </h1>
        }
        <br />
        <button onClick={this.hideButtonDiv}>Esconda Div</button> 
        <button onClick={this.showButtonDiv}>Mostra Div</button>
        {
          this.state.showDiv &&
          <div>
            Olá, div!
          </div>
        }
        <br />
        <button onClick={this.hideButtonH2}>Esconda H2</button> 
        <button onClick={this.showButtonH2}>Mostra H2</button>
        {
          this.state.showH2 &&
          <h2>
            Olá, H2!
          </h2>
        }
        <br />
        <button onClick={this.hideButtonH3}>Mostra/Esconde H3</button>         
        {
          this.state.showH3 &&
          <h3>
            Olá, H3!
          </h3>
        }
        <br />
        <button onClick={this.hideButtonB}>Mostra/Esconde B</button>         
        {
          this.state.showB &&
          <h3>
            Olá, B!
          </h3>
        }

      </div>
    )
  }  
}

export default App;
